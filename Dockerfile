FROM ubuntu:latest

# Requered to automate instalation of tzdata package, which is gcovr dependency
# Passing TimeZone argument in Ubuntu settings
ENV TZ=Asia/Tokyo
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && apt-get install -y build-essential
RUN apt-get install -y lcov
RUN apt-get install -y gcovr
RUN apt-get install -y clang-format